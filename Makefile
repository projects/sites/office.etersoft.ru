VERSION=10.08
DISKDIR=disk
SITEURL=http://download.etersoft.ru/pub/Etersoft/OFFICE@Etersoft
ISONAME=dvd.iso
LOCAL_PATH="/var/ftp/pub/Etersoft/OFFICE@Etersoft"

POJECT_FILES= \
    Project/contact.html \
    Project/database.html \
    Project/economy.html \
    Project/education.html \
    Project/freesoft.html \
    Project/games.html \
    Project/graphics.html \
    Project/internet.html \
    Project/legal.html \
    Project/list.html \
    Project/migration.html \
    Project/multimedia.html \
    Project/office.html \
    Project/prebuy.html \
    Project/scheme.html \
    Project/security.html \
    Project/service.html \
    Project/template.html \
    Project/tools.html

.PHONY: version disk iso burn

all: disk iso

disk:
	@echo Create Software
	@mkdir -p "$(DISKDIR)"
	@cp -avLT "$(LOCAL_PATH)/$(VERSION)/" "$(DISKDIR)/Software"

	@echo Create Project
	@mkdir -p "$(DISKDIR)/Project"
	@mkdir -p "$(DISKDIR)/Project/css"
	@cp -a Project/css/style.css "$(DISKDIR)/Project/css/"
	@cp -a Project/css/license.txt "$(DISKDIR)/Project/css/"
	@cp -a Project/css/images "$(DISKDIR)/Project/css/"
	@for i in $(POJECT_FILES); do \
	    cp -a $$i "$(DISKDIR)/Project/" ; \
	    sed -i -e "s|href=\"$(SITEURL)/$(VERSION)|href=\"../Software|g" "$(DISKDIR)/$$i"; \
	done
	@cp index.html "$(DISKDIR)/"

	@echo Create Autorun
	@mkdir -p "$(DISKDIR)"
	@cp autorun.inf "$(DISKDIR)/"

version:
	@for i in $(POJECT_FILES); do \
	    echo Set version $(VERSION) to $$i; \
	    sed -i -e "s!$(SITEURL)/[^/]*/!$(SITEURL)/$(VERSION)/!ig" "$$i"; \
	done

iso: $(DISKDIR)
	@echo Create Iso
	@mkisofs -r -J -o "$(ISONAME)" "$(DISKDIR)"

burn:
	@echo Burn Iso
	@growisofs -dvd-compat -Z /dev/dvd="$(ISONAME)"

clean:
	rm -rf "$(DISKDIR)"

distclean: clean
	rm -f "$(ISONAME)"
